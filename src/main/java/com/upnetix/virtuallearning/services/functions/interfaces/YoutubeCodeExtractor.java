package com.upnetix.virtuallearning.services.functions.interfaces;

public interface YoutubeCodeExtractor {

    String extractCode(String videoUrl);

}

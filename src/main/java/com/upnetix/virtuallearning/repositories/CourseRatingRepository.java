package com.upnetix.virtuallearning.repositories;

import com.upnetix.virtuallearning.models.Course;
import com.upnetix.virtuallearning.models.CourseRating;
import com.upnetix.virtuallearning.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CourseRatingRepository extends JpaRepository<CourseRating, Long> {
}

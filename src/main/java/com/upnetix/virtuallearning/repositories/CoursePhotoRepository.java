package com.upnetix.virtuallearning.repositories;

import com.upnetix.virtuallearning.models.CoursePhoto;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CoursePhotoRepository extends JpaRepository<CoursePhoto,Long> {
}

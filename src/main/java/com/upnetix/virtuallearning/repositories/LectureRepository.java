package com.upnetix.virtuallearning.repositories;
import com.upnetix.virtuallearning.models.Lecture;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface LectureRepository extends JpaRepository<Lecture, Long> {
    Lecture getById(long id);

    @Override
    List<Lecture> findAll();
}

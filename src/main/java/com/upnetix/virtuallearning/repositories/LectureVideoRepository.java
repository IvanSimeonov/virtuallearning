package com.upnetix.virtuallearning.repositories;

import com.upnetix.virtuallearning.models.LectureVideo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LectureVideoRepository extends JpaRepository<LectureVideo, Long> {
}

package com.upnetix.virtuallearning.repositories;

import com.upnetix.virtuallearning.models.Topic;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TopicRepository extends JpaRepository<Topic,Long> {

    Topic getTopicByName(String name);

}

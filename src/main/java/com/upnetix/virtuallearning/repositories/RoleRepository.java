package com.upnetix.virtuallearning.repositories;

import com.upnetix.virtuallearning.models.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role,Long> {


}

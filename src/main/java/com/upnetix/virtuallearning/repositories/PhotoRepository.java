package com.upnetix.virtuallearning.repositories;

import com.upnetix.virtuallearning.models.Photo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PhotoRepository extends JpaRepository<Photo,Long> {
}

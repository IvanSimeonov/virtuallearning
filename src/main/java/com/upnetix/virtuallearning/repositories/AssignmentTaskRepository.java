package com.upnetix.virtuallearning.repositories;

import com.upnetix.virtuallearning.models.AssignmentTask;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AssignmentTaskRepository extends JpaRepository<AssignmentTask,Long> {
}

package com.upnetix.virtuallearning;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VirtuallearningApplication {

    public static void main(String[] args) {
        SpringApplication.run(VirtuallearningApplication.class, args);
    }

}
